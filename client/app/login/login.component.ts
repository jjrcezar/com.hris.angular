import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { AuthService } from '../services/auth.service';
import { ToastComponent } from '../shared/toast/toast.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  username = new FormControl('', [Validators.minLength(6),
                                  Validators.maxLength(30)]);
  password = new FormControl('', [Validators.required,
                                  Validators.minLength(8),
                                  Validators.maxLength(16)]);
  remember = false;

  constructor(private auth: AuthService,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public toast: ToastComponent) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.username.setValue(params['username']);
    });

    if (this.auth.loggedIn) {
      this.router.navigate(['/']);
    }
    this.loginForm = this.formBuilder.group({
      username: this.username,
      password: this.password
    });
  }

  setClassUsername() {
    return { 'has-danger': !this.username.pristine && !this.username.valid };
  }
  setClassPassword() {
    return { 'has-danger': !this.password.pristine && !this.password.valid };
  }
  setClassRemember() {
    return { 'checkbox-helper-checked fa fa-check': this.remember, 'checkbox-helper': !this.remember };
  }

  login() {
    localStorage.setItem('remember', JSON.stringify(this.remember));
    this.auth.login(this.loginForm.value).subscribe(
      res => this.router.navigate(['/']),
      error => this.toast.setMessage('Incorrect username, email or password. Please try again.', 'danger')
    );
  }

}
