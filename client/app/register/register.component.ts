import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../services/user.service';
import { ToastComponent } from '../shared/toast/toast.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  firstname = new FormControl('', [Validators.required,
                                  Validators.minLength(2),
                                  Validators.maxLength(30),
                                  Validators.pattern('[a-zA-Z\\s]*')]);

  lastname = new FormControl('', [Validators.required,
                                  Validators.minLength(2),
                                  Validators.maxLength(30),
                                  Validators.pattern('[a-zA-Z\\s]*')]);

  email = new FormControl('', [Validators.required,
                               Validators.minLength(3),
                               Validators.maxLength(100),
                               Validators.pattern('^[a-zA-Z0-9_-\\s]*[@][a-zA-Z0-9_-\\s]*[.][a-zA-Z0-9_-\\s]+$')]);


  username = new FormControl('', [Validators.required,
                                  Validators.minLength(6),
                                  Validators.maxLength(30),
                                  Validators.pattern('[a-zA-Z0-9_-\\s]*')]);

  password = new FormControl('', [Validators.required,
                                  Validators.minLength(8),
                                  Validators.maxLength(16)]);

  role = new FormControl('', [Validators.required]);

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              public toast: ToastComponent,
              private userService: UserService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstname: this.firstname,
      lastname: this.lastname,
      email: this.email,
      username: this.username,
      password: this.password,
      role: this.role
    });
  }

  setClassFirstname() {
    return { 'has-danger': !this.firstname.pristine && !this.firstname.valid };
  }
  setClassLastname() {
    return { 'has-danger': !this.lastname.pristine && !this.lastname.valid };
  }
  setClassEmail() {
    return { 'has-danger': !this.email.pristine && !this.email.valid };
  }
  setClassUsername() {
    return { 'has-danger': !this.username.pristine && !this.username.valid };
  }
  setClassPassword() {
    return { 'has-danger': !this.password.pristine && !this.password.valid };
  }

  register() {
    this.userService.register(this.registerForm.value).subscribe(
      res => {
        this.toast.setMessage('Great! You have successfully registered.', 'success');
        this.router.navigate(['/login']);
      },
      error => {
         this.toast.setMessage('Email address is already in use. Please login to your account.', 'danger');
         this.router.navigate(['/login'], { queryParams: { username: this.email.value } });
      }
    );
  }
}
